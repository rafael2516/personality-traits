import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'personality-traits-frontend-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResultComponent implements OnInit {

  result = this.router.getCurrentNavigation()?.extras?.state?.result;

  constructor(public router: Router) {}

  ngOnInit(): void {
    if (!this.result) {
      this.router.navigate(['']);
    }
  }
}
