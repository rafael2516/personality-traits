import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultComponent } from './result.component';
import { RouterModule } from '@angular/router';
import { PersonalityResultModule } from '@personality-traits-frontend/web/result/ui/personality-result';


@NgModule({
  imports: [
    CommonModule,
    PersonalityResultModule,
    RouterModule.forChild([
      {
        path: '',
        component: ResultComponent
      }
    ])
  ],
  declarations: [ResultComponent],
  exports: [ResultComponent]
})
export class ResultModule {
}
