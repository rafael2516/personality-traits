import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Result } from '@personality-traits-frontend/web/shared/data-access/models';

@Component({
  selector: 'personality-traits-frontend-personality-result',
  templateUrl: './personality-result.component.html',
  styleUrls: ['./personality-result.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonalityResultComponent {

  @Input() result!: Result;

}
