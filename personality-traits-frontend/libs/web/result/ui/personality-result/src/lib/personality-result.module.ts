import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonalityResultComponent } from './personality-result.component';
import { TuiIslandModule } from '@taiga-ui/kit';

@NgModule({
  imports: [CommonModule, TuiIslandModule],
  declarations: [PersonalityResultComponent],
  exports: [PersonalityResultComponent]
})
export class PersonalityResultModule {
}
