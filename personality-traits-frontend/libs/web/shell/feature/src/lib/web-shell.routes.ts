import { Route } from '@angular/router';
import { LayoutComponent } from '@personality-traits-frontend/web/shell/ui/layout';

export const webShellRoutes: Route[] = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        loadChildren: async () => (await import('@personality-traits-frontend/web/home/feature')).HomeModule
      },
      {
        path: 'quiz',
        loadChildren: async () => (await import('@personality-traits-frontend/web/quiz/feature')).QuizModule
      },
      {
        path: 'result',
        loadChildren: async () => (await import('@personality-traits-frontend/web/result/feature')).ResultModule
      },
    ]
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full'
  }
];
