import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '@personality-traits-frontend/web/shell/ui/layout';
import { RouterModule } from '@angular/router';
import { webShellRoutes } from './web-shell.routes';
import { QuestionsApiService } from '@personality-traits-frontend/web/shared/data-access/api';

@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    RouterModule.forRoot(webShellRoutes, {
      scrollPositionRestoration: 'top'
    }),
  ],
  providers: [
    QuestionsApiService,
  ],
  exports: [RouterModule],
})
export class WebShellModule {}
