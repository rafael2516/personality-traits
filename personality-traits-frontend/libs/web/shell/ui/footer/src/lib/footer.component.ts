import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'personality-traits-frontend-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent {}
