import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'personality-traits-frontend-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopBarComponent {}
