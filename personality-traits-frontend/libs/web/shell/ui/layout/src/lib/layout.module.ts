import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { FooterModule } from '@personality-traits-frontend/web/shell/ui/footer';
import { MainViewModule } from '@personality-traits-frontend/web/shell/ui/main-view';
import { TopBarModule } from '@personality-traits-frontend/web/shell/ui/top-bar';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FooterModule,
    MainViewModule,
    TopBarModule
  ],
  declarations: [LayoutComponent],
  exports: [LayoutComponent],
})
export class LayoutModule {}
