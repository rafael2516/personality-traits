import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'personality-traits-frontend-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LayoutComponent {}
