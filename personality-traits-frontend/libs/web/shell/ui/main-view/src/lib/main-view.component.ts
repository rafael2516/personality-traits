import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'personality-traits-frontend-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainViewComponent  {}
