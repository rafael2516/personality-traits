import { Inject, Injectable } from '@angular/core';
import { APP_CONFIG, AppConfig } from '@personality-traits-frontend/web/shared/app-config';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Question, Result } from '@personality-traits-frontend/web/shared/data-access/models';
import { RadioQuestion } from '@personality-traits-frontend/web/quiz/data-access';
import { map } from 'rxjs/operators';


@Injectable()
export class QuestionsApiService {

  constructor(@Inject(APP_CONFIG) private appConfig: AppConfig, private http: HttpClient) {}

  getAllQuestions(): Observable<RadioQuestion[]>{
    return this.http.get<Question[]>(`${this.appConfig.baseURL}/questions`).pipe(
      map((questions: Question[]) =>
        questions.map((question) => new RadioQuestion({
          key: question.key,
          label: question.title,
          options: question.options
            .map((option) => ({value: option.value, description: option.title}))
            .sort((a, b) => b.value - a.value),
          order: question.position
        }))
      ),
    );
  }

  getResult(request: { value: number; key: string }[]): Observable<Result> {
    let params = new HttpParams();
    request.forEach(({ value, key }) => {
      params = params.append(key, value);
    })
    return this.http.get<Result>(`${this.appConfig.baseURL}/result`, { params });
  }
}


