export interface ResponseOption {
  title: string;
  value: number;
}
