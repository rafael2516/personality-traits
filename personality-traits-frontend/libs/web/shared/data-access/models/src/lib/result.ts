export interface Result {
  trait: string;
  description: string;
}
