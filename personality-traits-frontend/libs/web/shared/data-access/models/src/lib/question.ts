import { ResponseOption } from './response-option';

export interface Question {
  title: string;
  position: number;
  key: string;
  options: ResponseOption[];
}
