import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { QuestionBase } from '@personality-traits-frontend/web/quiz/data-access';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'personality-traits-frontend-dynamic-form-question',
  templateUrl: './dynamic-form-question.component.html',
  styleUrls: ['./dynamic-form-question.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicFormQuestionComponent {

  @Input()
  question!: QuestionBase<string>;

  @Input()
  form!: FormGroup;

}
