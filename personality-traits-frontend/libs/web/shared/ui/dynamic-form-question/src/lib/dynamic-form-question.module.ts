import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormQuestionComponent } from './dynamic-form-question.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TuiFieldErrorModule, TuiRadioListModule, TuiRadioModule } from '@taiga-ui/kit';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, TuiRadioModule, TuiRadioListModule, TuiFieldErrorModule],
  declarations: [DynamicFormQuestionComponent],
  exports: [DynamicFormQuestionComponent],
})
export class DynamicFormQuestionModule {}
