import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizComponent } from './quiz.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { QuizQuestionModule } from '@personality-traits-frontend/web/quiz/ui/quiz-question';
import { QuizResolver } from './quiz.resolver';

@NgModule({
  imports: [
    CommonModule,
    QuizQuestionModule,
    RouterModule.forChild([
      {
        path: '',
        component: QuizComponent,
        resolve: {
          questions: QuizResolver
        }
      }
    ]),
    ReactiveFormsModule
  ],
  declarations: [QuizComponent],
  exports: [QuizComponent]
})
export class QuizModule {
}
