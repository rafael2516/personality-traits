import { Resolve } from '@angular/router';
import { Injectable } from '@angular/core';
import { QuestionsApiService } from '@personality-traits-frontend/web/shared/data-access/api';
import { Observable } from 'rxjs';
import { QuestionBase } from '@personality-traits-frontend/web/quiz/data-access';

@Injectable({ providedIn: 'root' })
export class QuizResolver implements Resolve<QuestionBase<string>[]> {

  constructor(private questionApiService: QuestionsApiService) {
  }

  resolve(): Observable<QuestionBase<string>[]> {
    return this.questionApiService.getAllQuestions();
  }

}
