import { Inject, Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TuiNotification, TuiNotificationsService } from '@taiga-ui/core';
import { RadioQuestion } from './question-radio';

@Injectable()
export class QuestionControlService {

  private parentForm = new FormGroup({});
  private currentStep = 0;
  private maxSteps = 0;


  constructor(@Inject(TuiNotificationsService)
              private readonly notificationsService: TuiNotificationsService) {
  }

  get getCurrentStep(): number {
    return this.currentStep;
  }

  buildForm(questions: RadioQuestion[]): void {
    const group: any = {};

    questions.forEach(question =>
      group[question.key] = question.required
        ? new FormControl(question.value || '', Validators.required)
        : new FormControl(question.value || '')
    );

    this.maxSteps = questions.length;
    this.parentForm = new FormGroup(group);
  }

  getParentForm(): FormGroup {
    return this.parentForm;
  }

  getMaxSteps(): number {
    return this.maxSteps;
  }

  next(): void {
    if (!this.getCurrentChildForm().valid) {
      this.notificationsService
        .show('Please insert an option!', {
          status: TuiNotification.Warning,
          label: 'Warning!'
        })
        .subscribe();
      return;
    }
    this.currentStep += 1;
  }

  prev(): void {
    this.currentStep -= 1;
  }

  getCurrentChildForm(): FormGroup {
    return this.getGroupAt();
  }

  private getGroupAt(): FormGroup {
    const groups = Object.keys(this.parentForm.controls).map((groupName: string) =>
      this.parentForm.get(groupName)
    ) as FormGroup[];

    return groups[this.currentStep];
  }
}
