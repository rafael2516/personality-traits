import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { QuestionControlService, RadioQuestion } from '@personality-traits-frontend/web/quiz/data-access';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { QuestionsApiService } from '@personality-traits-frontend/web/shared/data-access/api';


@Component({
  selector: 'personality-traits-frontend-quiz-question',
  templateUrl: './quiz-question.component.html',
  styleUrls: ['./quiz-question.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [QuestionControlService]
})
export class QuizQuestionComponent implements OnInit {

  @Input()
  questions: RadioQuestion[] | null = [];

  form!: FormGroup;

  constructor(public qcs: QuestionControlService,
              private questionApiService: QuestionsApiService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.qcs.buildForm(this.questions as RadioQuestion[]);
    this.form = this.qcs.getParentForm();
  }

  onSubmit(): void {
    const request = Object.keys(this.form.controls).map((groupName: string) => ({
        key: groupName,
        value: (this.form.get(groupName) as FormGroup).value.value
    }));

    this.questionApiService.getResult(request).subscribe((result) =>
      this.router.navigate(['/result'], { state: { result } })
    )
  }

  displaySubmitButton(): boolean {
    if (!this.questions) {
      return false;
    }

    return this.qcs.getCurrentStep === this.questions?.length - 1;
  }

}
