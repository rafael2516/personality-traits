import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizQuestionComponent } from './quiz-question.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DynamicFormQuestionModule } from '@personality-traits-frontend/web/shared/ui/dynamic-form-question';
import { TuiButtonModule } from '@taiga-ui/core';
import { TuiIslandModule, TuiProgressModule } from '@taiga-ui/kit';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DynamicFormQuestionModule,
    TuiButtonModule,
    TuiIslandModule,
    TuiProgressModule
  ],
  declarations: [QuizQuestionComponent],
  exports: [QuizQuestionComponent]
})
export class QuizQuestionModule {
}
