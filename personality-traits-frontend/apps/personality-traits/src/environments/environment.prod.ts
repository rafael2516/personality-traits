import { AppConfig } from '@personality-traits-frontend/web/shared/app-config';

export const environment: AppConfig = {
  production: true,
  baseURL: 'http://localhost:8080'
};
