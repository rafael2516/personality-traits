import { Component } from '@angular/core';

@Component({
  selector: 'personality-traits-frontend-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {}
