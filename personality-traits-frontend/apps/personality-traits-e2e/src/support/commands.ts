// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// eslint-disable-next-line @typescript-eslint/no-namespace
declare namespace Cypress {

  interface Chainable<Subject> {
    checkAndNext(index: number): void;
  }
  interface Chainable<Subject> {
    checkAndSubmit(index: number): void;
  }
}

Cypress.Commands.add('checkAndNext',(index) => {
  cy.get(`#radio-option-${index}`).check();

  cy.get('[data-cy=next]').click();
});

Cypress.Commands.add('checkAndSubmit',(index) => {
  cy.get('[data-cy=submit]').should('be.disabled');

  cy.get(`#radio-option-${index}`).check();

  cy.get('[data-cy=submit]').should('be.enabled');

  cy.get('[data-cy=submit]').click();
})
