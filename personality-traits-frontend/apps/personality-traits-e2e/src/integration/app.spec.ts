
describe('personality-traits', () => {
  beforeEach(() => cy.visit('/'));

  it('should display quiz', () => {
    cy.get('[data-cy=home-take-quiz]').click();

    cy.url().should('eq', 'http://localhost:4200/quiz')
  });

  it('Should not be able to go back on first step', () => {
    cy.get('[data-cy=home-take-quiz]').click();

    cy.url().should('eq', 'http://localhost:4200/quiz')

    cy.get('[type="radio"]').first().check();

    cy.get('[data-cy=previous]').should('be.disabled');

  });

  it('Should not be able to go forward  without filling required fields', () => {
    cy.get('[data-cy=home-take-quiz]').click();

    cy.url().should('eq', 'http://localhost:4200/quiz')

    cy.get('[data-cy=next]').click();

    cy.get('label[automation-id=tui-notification-alert__heading]').contains("Warning!");
    cy.get('div[automation-id=tui-notification-alert__content]').contains("Please insert an option!");

  });


  it('Highly introverted', () => {
    cy.get('[data-cy=home-take-quiz]').click();

    cy.url().should('eq', 'http://localhost:4200/quiz')

    cy.checkAndNext(0);
    cy.checkAndNext(0);
    cy.checkAndNext(2);
    cy.checkAndNext(4);
    cy.checkAndNext(0);
    cy.checkAndNext(4);
    cy.checkAndNext(4);
    cy.checkAndNext(1);
    cy.checkAndNext(1);
    cy.checkAndNext(0);
    cy.checkAndNext(0);
    cy.checkAndNext(4);
    cy.checkAndNext(0);
    cy.checkAndNext(0);
    cy.checkAndNext(0);
    cy.checkAndNext(0);
    cy.checkAndNext(3);
    cy.checkAndSubmit(1);

    cy.url().should('eq', 'http://localhost:4200/result')

    cy.get('[data-cy=result]').contains('Highly introverted');

  });

  it('Highly extraverted', () => {
    cy.get('[data-cy=home-take-quiz]').click();

    cy.url().should('eq', 'http://localhost:4200/quiz')

    cy.checkAndNext(0);
    cy.checkAndNext(4);
    cy.checkAndNext(2);
    cy.checkAndNext(0);
    cy.checkAndNext(4);
    cy.checkAndNext(2);
    cy.checkAndNext(0);
    cy.checkAndNext(1);
    cy.checkAndNext(2);
    cy.checkAndNext(4);
    cy.checkAndNext(4);
    cy.checkAndNext(0);
    cy.checkAndNext(4);
    cy.checkAndNext(4);
    cy.checkAndNext(2);
    cy.checkAndNext(3);
    cy.checkAndNext(1);
    cy.checkAndSubmit(3);

    cy.url().should('eq', 'http://localhost:4200/result')

    cy.get('[data-cy=result]').contains('Highly extraverted');

  });


  it('Moderated', () => {
    cy.get('[data-cy=home-take-quiz]').click();

    cy.url().should('eq', 'http://localhost:4200/quiz')

    cy.checkAndNext(0);
    cy.checkAndNext(0);
    cy.checkAndNext(2);
    cy.checkAndNext(4);
    cy.checkAndNext(0);
    cy.checkAndNext(4);
    cy.checkAndNext(4);
    cy.checkAndNext(3);
    cy.checkAndNext(2);
    cy.checkAndNext(1);
    cy.checkAndNext(1);
    cy.checkAndNext(4);
    cy.checkAndNext(0);
    cy.checkAndNext(0);
    cy.checkAndNext(2);
    cy.checkAndNext(1);
    cy.checkAndNext(3);
    cy.checkAndSubmit(1);

    cy.url().should('eq', 'http://localhost:4200/result')

    cy.get('[data-cy=result]').contains('Moderate');

  });

});
