package rvp.personalitytraits.responseOptions.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import rvp.personalitytraits.responseOptions.ResponseOption;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseOptionDTO {

  private String title;
  private int value;

  public static ResponseOptionDTO of(ResponseOption option) {
    return new ResponseOptionDTO(option.getTitle(), option.getValue());
  }
}
