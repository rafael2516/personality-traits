package rvp.personalitytraits.responseOptions;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ResponseOptionRepository extends CrudRepository<ResponseOption, UUID> {
}
