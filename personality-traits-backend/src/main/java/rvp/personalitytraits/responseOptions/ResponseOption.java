package rvp.personalitytraits.responseOptions;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "response_option")
@AllArgsConstructor
@NoArgsConstructor(force = true)
@ToString
public class ResponseOption {

  @Id
  @GeneratedValue
  @Column(columnDefinition = "BINARY(16)")
  @Getter
  private UUID id;

  @Getter
  @Setter
  private String title;

  @Getter
  @Setter
  private int value;

  public ResponseOption(String title, int value) {
    this.title = title;
    this.value = value;
  }
}
