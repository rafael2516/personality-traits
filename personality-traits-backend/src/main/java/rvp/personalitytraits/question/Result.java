package rvp.personalitytraits.question;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Result {

    private String trait;
    private String description;

    public Result(Trait trait) {
        this.trait = trait.getCode();
        this.description = trait.getDescription();
    }
}
