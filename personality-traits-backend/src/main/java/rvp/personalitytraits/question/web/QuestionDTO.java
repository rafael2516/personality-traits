package rvp.personalitytraits.question.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import rvp.personalitytraits.question.Question;
import rvp.personalitytraits.responseOptions.web.ResponseOptionDTO;

import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDTO {

  private String title;
  private int position;
  private String key;
  private Set<ResponseOptionDTO> options;

  public static QuestionDTO of(Question question) {

    return new QuestionDTO(question.getTitle(), question.getPosition(), question.getKey(),
            question.getResponseOptions().stream().map(ResponseOptionDTO::of).collect(Collectors.toSet()));
  }
}
