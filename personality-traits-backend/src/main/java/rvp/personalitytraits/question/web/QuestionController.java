package rvp.personalitytraits.question.web;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rvp.personalitytraits.question.QuestionService;
import rvp.personalitytraits.question.Result;

import java.util.Map;
import java.util.stream.Stream;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequiredArgsConstructor
public class QuestionController {

  private final @NonNull QuestionService questionService;

  @GetMapping("/questions")
  public Stream<QuestionDTO> allQuestions() {
    return this.questionService.findAll().map(QuestionDTO::of).stream();
  }

  @GetMapping("/result")
  public ResponseEntity<Result> processResult(@RequestParam Map<String, String> params) {
    return ResponseEntity.ok(this.questionService.processResult(params));
  }
}
