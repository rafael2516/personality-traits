package rvp.personalitytraits.question;

import java.io.Serial;

public class QuestionException extends RuntimeException {

	@Serial
	private static final long serialVersionUID = 5076506209061485363L;

	public QuestionException(String message) {
		super(message);
	}
}
