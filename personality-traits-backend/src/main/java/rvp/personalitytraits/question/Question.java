package rvp.personalitytraits.question;

import lombok.*;
import rvp.personalitytraits.responseOptions.ResponseOption;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "question")
@AllArgsConstructor
@NoArgsConstructor(force = true)
@ToString
public class Question {

  @Id
  @GeneratedValue
  @Column(columnDefinition = "BINARY(16)")
  @Getter
  private UUID id;


  @Column(unique = true)
  @Getter
  @Setter
  private String title;

  @Getter
  @Setter
  @Column(unique = true)
  private int position;

  @Getter
  @Setter
  @Column(unique = true, nullable = false)
  private String key;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
  @JoinColumn(name = "question_id")
  @Getter
  @Setter
  private Set<ResponseOption> responseOptions;

  public Question(String title, int position, String key) {
    this.title = title;
    this.key = key;
    this.position = position;
    this.responseOptions = new HashSet<>();
  }
}
