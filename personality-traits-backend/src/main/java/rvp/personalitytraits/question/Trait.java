package rvp.personalitytraits.question;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Trait {

    MODERATE("Moderate", " Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor " +
            "incididunt ut labore et dolore magna aliqua." ),
    HIGHLY_EXTRAVERTED("Highly extraverted", "An outgoing, gregarious person who thrives in dynamic " +
            "environments and seeks to maximize social engagement."),
    HIGHLY_INTROVERTED("Highly introverted", "An introvert is a person with qualities of a personality type known as " +
            "introversion, which means that they feel more comfortable focusing on their inner thoughts and ideas, rather" +
            " than what’s happening externally. They enjoy spending time with just one or two people, rather than large " +
            "groups or crowds." );

    @Getter private String code;
    @Getter private String description;
}
