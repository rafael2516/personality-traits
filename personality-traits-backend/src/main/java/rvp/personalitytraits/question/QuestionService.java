package rvp.personalitytraits.question;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Service;
import rvp.personalitytraits.responseOptions.ResponseOption;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class QuestionService {

  private final @NonNull QuestionRepository questionRepository;

  public Streamable<Question> findAll() {
    return this.questionRepository.findAll();
  }

  public Question createQuestion(String title, int position, String key, Set<ResponseOption> options) {

    Question question = new Question(title, position, key);
    question.setResponseOptions(options);

    Question savedQuestion = questionRepository.save(question);

    log.info("Created question with the given title: " + savedQuestion.getTitle());

    return savedQuestion;
  }

  public Result processResult(Map<String, String> params) {
    List<Integer> hashMap = params.values()
            .stream()
            .mapToInt(Integer::parseInt)
            .boxed()
            .collect(Collectors.toList());

    int count = (int) this.questionRepository.count();
    if (hashMap.size() != count) {
      throw new QuestionException("Params do not match with number of questions");
    }

    // Ideally each question would contain a column specifying in which step belongs
    int stepOne = hashMap.get(0) + hashMap.get(3);
    int stepTwo = hashMap.get(1) + hashMap.get(4) +hashMap.get(6) + hashMap.get(7) + hashMap.get(9) +
            hashMap.get(10) + hashMap.get(12) + hashMap.get(13) + hashMap.get(15) + hashMap.get(17) ;

    int total = 12 - stepOne + stepTwo;

    log.info("Result: " + total);

    if (total > 48) {
      return new Result(Trait.HIGHLY_INTROVERTED);
    } else if (total < 24) {
      return new Result(Trait.HIGHLY_EXTRAVERTED);
    } else {
      return  new Result(Trait.MODERATE);
    }
  }
}
