package rvp.personalitytraits.question;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import rvp.personalitytraits.core.DataInitializer;
import rvp.personalitytraits.responseOptions.ResponseOption;

import java.util.Set;

@Component
@RequiredArgsConstructor
@Order(200)
@Slf4j
public class QuestionInitializer implements DataInitializer {

  private final @NonNull QuestionService questionService;

  @Override
  public void initialize() {

    log.debug("Creating questions...");


    questionService.createQuestion("Are you inclined to keep in the background on social occasions?", 1, "question-1",  Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Do you like to mix socially with people?", 2,"question-2", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Do you sometimes feel happy, sometimes depressed, without any apparent reason?", 3,"question-3", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Are you inclined to limit your acquaintances to a select few?", 4, "question-4", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Do you like to have many social engagements?", 5,"question-5", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Do you have frequent ups and downs in mood, either with or without apparent cause?", 6, "question-6",Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Would you rate yourself as a happy-go-lucky individual?", 7, "question-7",Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Can you usually let yourself go and have a good time at a party?", 8, "question-8",Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Are you inclined to be moody?", 9, "question-9",Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Would you be very unhappy if you were prevented from making numerous social contacts?", 10, "question-10",Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Do you usually take the initiative in making new friends?", 11,"question-11", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Does your mind often wander while you are trying to concentrate?", 12, "question-12",Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Do you like to play pranks upon others?", 13,"question-13", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Are you usually a \"good mixer?\"", 14, "question-14", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Are you sometimes bubbling over with energy and sometimes very sluggish?", 15,"question-15", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Do you often \"have the time of your life\" at social affairs?", 16, "question-16",Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Are you frequently \"lost in thought\" even when you should be taking part in a conversation?", 17,"question-17", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));
    questionService.createQuestion("Do you derive more satisfaction from social activities than from anything else?", 18,"question-18", Set.of(
            new ResponseOption("Strongly Disagree", 1),
            new ResponseOption("Disagree", 2),
            new ResponseOption("Undecided ", 3),
            new ResponseOption("Agree", 4),
            new ResponseOption("Strongly Agree", 5)
    ));

  }
}
