package rvp.personalitytraits;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonalityTraitsApplication {

  public static void main(String[] args) {
    SpringApplication.run(PersonalityTraitsApplication.class, args);
  }

}
