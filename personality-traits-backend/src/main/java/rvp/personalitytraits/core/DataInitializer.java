package rvp.personalitytraits.core;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public interface DataInitializer {

  default void preInitialize() {
  }

  @Transactional
  void initialize();

  default void postInitialize() {
  }
}
