package rvp.personalitytraits.core;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.support.AopUtils;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Internal component triggering the initialization of all {@link DataInitializer} instances registered in the
 * {@link ApplicationContext}.
 */
@Component
@RequiredArgsConstructor
@Slf4j
@Profile("!prod")
@Order(100)
class DataInitializerInvoker implements ApplicationRunner {

  private final @NonNull List<DataInitializer> initializers;

  @Override
  public void run(@Nullable ApplicationArguments args) {

    initializers.stream()
            .peek(it -> log.info("Invoking " + AopUtils.getTargetClass(it)))
            .forEach(it -> {

              it.preInitialize();
              it.initialize();
              it.postInitialize();
            });
  }

}
